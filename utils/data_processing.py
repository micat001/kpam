import os
import argparse
import yaml
import numpy as np
import cv2
import attr

from typing import List, Set

# epsilon for testing whether a number is close to zero
_EPS = np.finfo(float).eps * 4.0


@attr.s
class PixelCoord(object):
    """
    A small struct used to index in image.
    Note that x is the column index, y is row index
    """

    x = 0
    y = 0

    @property
    def row_location(self):  # type: (PixelCoord) -> int
        return self.y

    @property
    def col_location(self):  # type: (PixelCoord) -> int
        return self.x


def quaternion_matrix(quaternion):
    """Return homogeneous rotation matrix from quaternion.

    >>> M = quaternion_matrix([0.99810947, 0.06146124, 0, 0])
    >>> np.allclose(M, rotation_matrix(0.123, [1, 0, 0]))
    True
    >>> M = quaternion_matrix([1, 0, 0, 0])
    >>> np.allclose(M, np.identity(4))
    True
    >>> M = quaternion_matrix([0, 1, 0, 0])
    >>> np.allclose(M, np.diag([1, -1, -1, 1]))
    True

    """
    q = np.array(quaternion, dtype=np.float64, copy=True)
    n = np.dot(q, q)
    if n < _EPS:
        return np.identity(4)
    q *= np.sqrt(2.0 / n)
    q = np.outer(q, q)
    return np.array(
        [
            [1.0 - q[2, 2] - q[3, 3], q[1, 2] - q[3, 0], q[1, 3] + q[2, 0], 0.0],
            [q[1, 2] + q[3, 0], 1.0 - q[1, 1] - q[3, 3], q[2, 3] - q[1, 0], 0.0],
            [q[1, 3] - q[2, 0], q[2, 3] + q[1, 0], 1.0 - q[1, 1] - q[2, 2], 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ]
    )


def get_keypoint_name_list(annotation_dict) -> List[str]:
    """
    Given the first annotation dict, return the list of keypoints
    that will be used for projection.
    Used to ensure that the order the keypoint
    :param annotation_dict: The dict from the annotation yaml
    :return: A list of keypoint in the dictionary
    """
    assert "keypoints" in annotation_dict
    keypoint_name_list: List[str] = []
    for keypoint_name in annotation_dict["keypoints"]:
        keypoint_name_list.append(keypoint_name)

    # OK
    return keypoint_name_list


def get_keypoint_in_world(mesh_keypoint_map) -> np.ndarray:
    """
    Read the keypoint from the config and save them into an np.ndarray
    The keypoint is expressed in homogeneous coordinate (padding with 1)
    :param mesh_keypoint_map:
    :return:np.ndarray at (4, num_keypoints)
    """
    keypoint_in_world = mesh_keypoint_map["keypoint_world_position"]
    keypoint_np = np.ones((4, len(keypoint_in_world)))
    offset = 0
    for keypoint in keypoint_in_world:
        for i in range(3):
            keypoint_np[i, offset] = keypoint[i]
        offset = offset + 1
    return keypoint_np


# The group of method used for bounding box processing.
# Mainly used to rectify the bounding box
def first_nonzero_idx(binary_array, reversed):  # type: (np.ndarray, bool) -> int
    """
    Get the index of the first element in an array that is not zero
    reversed means whether the binary_array should be reversed
    :param binary_array: A 1-D numpy array
    :param reversed:
    :return: The index to the first non-zero element
    """
    start = 0
    end = binary_array.size
    step = 1
    if reversed:
        start = binary_array.size - 1
        end = -1
        step = -1

    # The iteration
    for i in range(start, end, step):
        if binary_array[i] > 0:
            return i

    # Everything is zero
    return None


def mask2bbox(mask_img):  # type: (np.ndarray) -> (PixelCoord, PixelCoord)
    """
    Given an object binary mask, get the tight object bounding box
    as a tuple contains top_left and bottom_right pixel coord
    :param mask_img: (height, width, 3) mask image
    :return: A tuple contains top_left and bottom_right pixel coord
    """
    binary_mask = mask_img.max(axis=2)
    n_rows, n_cols = binary_mask.shape
    # Compute sum over the row and compute the left and right
    mask_rowsum = np.sum(binary_mask, axis=0, keepdims=False)
    assert mask_rowsum.size == n_cols
    left = first_nonzero_idx(mask_rowsum, False)
    right = first_nonzero_idx(mask_rowsum, True)

    # Compute sum over the col and compute the top and bottom
    mask_colsum = np.sum(binary_mask, axis=1)
    assert mask_colsum.size == n_rows
    top = first_nonzero_idx(mask_colsum, False)
    bottom = first_nonzero_idx(mask_colsum, True)

    # Ok
    top_left = PixelCoord()
    top_left.x = left
    top_left.y = top
    bottom_right = PixelCoord()
    bottom_right.x = right
    bottom_right.y = bottom
    return top_left, bottom_right


def point2pixel(keypoint_in_camera: np.ndarray, camera_config_map) -> np.ndarray:
    """
    Given keypoint in camera frame, project them into image
    space and compute the depth value expressed in [mm]
    :param keypoint_in_camera: (4, n_keypoint) keypoint expressed in camera frame in meter
    :return: (3, n_keypoint) where (xy, :) is pixel location and (z, :) is depth in mm
    """
    focal_x: float = camera_config_map["camera_matrix"]["data"][0]
    focal_y: float = camera_config_map["camera_matrix"]["data"][4]
    principal_x: float = camera_config_map["camera_matrix"]["data"][2]
    principal_y: float = camera_config_map["camera_matrix"]["data"][5]

    assert len(keypoint_in_camera.shape) == 2
    n_keypoint: int = keypoint_in_camera.shape[1]
    xy_depth = np.zeros((3, n_keypoint), dtype=int)
    xy_depth[0, :] = (
        np.divide(keypoint_in_camera[0, :], keypoint_in_camera[2, :]) * focal_x
        + principal_x
    ).astype(int)
    xy_depth[1, :] = (
        np.divide(keypoint_in_camera[1, :], keypoint_in_camera[2, :]) * focal_y
        + principal_y
    ).astype(int)
    xy_depth[2, :] = (1000.0 * keypoint_in_camera[2, :]).astype(int)
    return xy_depth


def camera2world_from_map(camera2world_map) -> np.ndarray:
    """
    Get the transformation matrix from the storage map
    See ${processed_log_path}/processed/images/pose_data.yaml for an example
    :param camera2world_map:
    :return: The (4, 4) transform matrix from camera 2 world
    """
    # The rotation part
    camera2world_quat = [1, 0, 0, 0]
    camera2world_quat[0] = camera2world_map["quaternion"]["w"]
    camera2world_quat[1] = camera2world_map["quaternion"]["x"]
    camera2world_quat[2] = camera2world_map["quaternion"]["y"]
    camera2world_quat[3] = camera2world_map["quaternion"]["z"]
    camera2world_quat = np.asarray(camera2world_quat)
    camera2world_matrix = quaternion_matrix(camera2world_quat)

    # The linear part
    camera2world_matrix[0, 3] = camera2world_map["translation"]["x"]
    camera2world_matrix[1, 3] = camera2world_map["translation"]["y"]
    camera2world_matrix[2, 3] = camera2world_map["translation"]["z"]
    return camera2world_matrix


def world2camera_from_map(camera2world_map) -> np.ndarray:
    camera2world = camera2world_from_map(camera2world_map)
    return np.linalg.inv(camera2world)


def process_scene_image(
    image_data_map, keypoint_in_world: np.ndarray, camera_config_map
):
    """
    The processor for an single image. Given the camera pose and intrinsic,
    compute the keypoint in camera frame and its pixel location, and store
    them into the image_data_map. Note that the keypoint might be out of the image.
    :param image_data_map:
    :param keypoint_in_world: (4, n_keypoint) numpy ndarray for keypoint in world frame
    :return: None
    """
    camera2world_map = image_data_map["camera_to_world"]
    world2camera = world2camera_from_map(camera2world_map)

    # Do transform and save the keypoint in data map
    n_keypoints: int = keypoint_in_world.shape[1]
    keypoint_in_camera = world2camera.dot(keypoint_in_world)
    keypoint_in_camera_list = []
    for i in range(n_keypoints):
        keypoint_in_camera_list.append(
            [
                float(keypoint_in_camera[0, i]),
                float(keypoint_in_camera[1, i]),
                float(keypoint_in_camera[2, i]),
            ]
        )
    image_data_map["3d_keypoint_camera_frame"] = keypoint_in_camera_list

    # Project the keypoint into pixel and depth
    pixel_xy_depth = point2pixel(keypoint_in_camera, camera_config_map)
    pixel_xy_depth_list = []
    for i in range(n_keypoints):
        pixel_xy_depth_list.append(
            [
                int(pixel_xy_depth[0, i]),
                int(pixel_xy_depth[1, i]),
                int(pixel_xy_depth[2, i]),
            ]
        )
    image_data_map["keypoint_pixel_xy_depth"] = pixel_xy_depth_list


def process_scene_keypoints(
    pose_data_map, keypoint_in_world: np.ndarray, output_path: str, camera_config_map
):
    """
    The processor for scene which contains many images.
    Dispatch to image level processor and collect the result.
    :param pose_data_map: The data map loaded from pose yaml file
    :param keypoint_in_world: The data map loaded from keypoint yaml file
    :param output_path:
    :return: None
    """
    for image_idx in pose_data_map:
        image_data_map = pose_data_map[image_idx]
        process_scene_image(image_data_map, keypoint_in_world, camera_config_map)

    # Save the output as path
    with open(output_path, "w") as out_file:
        yaml.dump(pose_data_map, out_file)


def process_scene_bbox(scene_keypoint_map, scene_log_root: str, yaml_out_path: str):
    # Iterate over all image in the scene
    image_keys = scene_keypoint_map.keys()
    for img_key in image_keys:
        image_map = scene_keypoint_map[img_key]
        filename_str = image_map["depth_image_filename"][0:6]
        mask_name = filename_str + "_mask.png"
        mask_root = os.path.join(scene_log_root, "processed/image_masks")
        mask_path = os.path.join(mask_root, mask_name)

        # Load the mask
        mask_img = cv2.imread(mask_path)
        assert mask_img is not None
        top_left, bottom_right = mask2bbox(mask_img)

        # Save to map
        image_map["bbox_top_left_xy"] = [top_left.x, top_left.y]
        image_map["bbox_bottom_right_xy"] = [bottom_right.x, bottom_right.y]

    # Save the output as path
    with open(yaml_out_path, "w") as out_file:
        yaml.dump(scene_keypoint_map, out_file)


def process_annotation_dict(
    annotation_dict, keypoint_name_list, data_path, save_relative_path
):
    """
    Given a standalone annotation, make the image annotation at given scene
    The scene is only annotated the first time.
    :param annotation_dict:
    :param keypoint_name_list:
    :return:
    """

    scene_name: str = annotation_dict["scene_name"]
    scene_root_path = os.path.join(data_path, scene_name)
    scene_processed_path = os.path.join(scene_root_path, "processed")
    raw_image_root = os.path.join(scene_processed_path, "images")
    camera_config_path = os.path.join(raw_image_root, "camera_info.yaml")
    camera_config_map = yaml.safe_load(open(camera_config_path, "r"))

    assert os.path.exists(scene_root_path) and os.path.isdir(scene_root_path)
    assert os.path.exists(scene_processed_path) and os.path.isdir(scene_processed_path)

    # Check the existence of keypoints
    annotation_keypoints_dict = annotation_dict["keypoints"]
    for keypoint_name in keypoint_name_list:
        assert keypoint_name in annotation_keypoints_dict

    # Create a tmp keypoint.yaml
    tmp_keypoint_yaml_name = "director_keypoint_tmp.yaml"
    tmp_keypoint_yaml_path = os.path.join(scene_processed_path, tmp_keypoint_yaml_name)
    with open(tmp_keypoint_yaml_path, "w") as tmp_keypoint_yaml_file:
        keypoint_3d_position = []
        for keypoint_name in keypoint_name_list:
            keypoint_pos = annotation_keypoints_dict[keypoint_name]["position"]
            keypoint_3d_position.append(
                [float(keypoint_pos[0]), float(keypoint_pos[1]), float(keypoint_pos[2])]
            )

        # Save to yaml file
        tmp_keypoint_yaml_map = dict()
        tmp_keypoint_yaml_map["keypoint_world_position"] = keypoint_3d_position
        yaml.dump(tmp_keypoint_yaml_map, tmp_keypoint_yaml_file)

    # Do mesh3image keypoint projection
    tmp_imgkeypoint_yaml_name = "director_img_keypoint_tmp.yaml"
    tmp_imgkeypoint_yaml_path = os.path.join(
        scene_processed_path, tmp_imgkeypoint_yaml_name
    )

    # run keypoint_mesh2img:
    pose_data_path: str = os.path.join(raw_image_root, "pose_data.yaml")
    assert os.path.exists(pose_data_path)
    in_pose_file = open(pose_data_path, "r")
    pose_data_map = yaml.safe_load(in_pose_file)

    # Load the keypoint data
    keypoint_data_path = tmp_keypoint_yaml_path
    assert os.path.exists(keypoint_data_path)
    in_keypoint_file = open(keypoint_data_path, "r")
    keypoint_data_map = yaml.safe_load(in_keypoint_file)

    keypoint_in_world = get_keypoint_in_world(keypoint_data_map)

    # Do it
    output_path = tmp_imgkeypoint_yaml_path
    process_scene_keypoints(
        pose_data_map, keypoint_in_world, output_path, camera_config_map
    )

    # Compute bounding box
    img_keypoint_output_path = os.path.join(scene_processed_path, save_relative_path)
    # Check the existence of path
    assert os.path.exists(scene_root_path) and os.path.isdir(scene_root_path)
    assert os.path.exists(tmp_imgkeypoint_yaml_path)

    # Load the map and close the file
    pose_data_yaml_file = open(tmp_imgkeypoint_yaml_path, "r")
    pose_data_yaml = yaml.safe_load(pose_data_yaml_file)
    pose_data_yaml_file.close()

    process_scene_bbox(pose_data_yaml, scene_root_path, img_keypoint_output_path)

    # # Clean up tmp file
    if os.path.exists(tmp_keypoint_yaml_path):
        os.remove(tmp_keypoint_yaml_path)
    if os.path.exists(tmp_imgkeypoint_yaml_path):
        os.remove(tmp_imgkeypoint_yaml_path)
